package ru.fnight.integrationbackpack;

public enum Algorithm {

    FAST("Жадный алгоритм"),
    SLOW("Полный перебор");

    private String description;

    Algorithm(String envUrl) {
        this.description = envUrl;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }
}
