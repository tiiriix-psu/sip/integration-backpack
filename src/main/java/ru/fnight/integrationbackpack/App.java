package ru.fnight.integrationbackpack;


import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.fnight.integrationbackpack.controllers.WindowController;

public class App extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(Class.class.getResource("/fxml/window.fxml"));
        Parent root = loader.load();
        stage.setTitle("Интеграция");
        stage.setScene(new Scene(root));
        WindowController controller = loader.getController();
//        int[] solve = BackpackFast.solve(100, new int[]{1,2,3}, new int[]{4,5,67,7});
        stage.show();
    }
}