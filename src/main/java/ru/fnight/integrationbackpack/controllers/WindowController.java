package ru.fnight.integrationbackpack.controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.fnight.integrationbackpack.Algorithm;
import ru.fnight.integrationbackpack.domains.Item;
import ru.fnight.libtaskbackpackfast.BackpackFast;
import ru.fnight.libtaskbackslow.BackpackSlow;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class WindowController {

    public TableView<Item> inputTable;
    public TableColumn<Item, Integer> priceInputCol;
    public TableColumn<Item, Integer> weightInputCol;

    public TableView<Item> outputTable;
    public TableColumn<Item, String> priceOutputCol;
    public TableColumn<Item, String> outputWeightCol;

    public TextField capacityText;
    public TextField weightText;
    public TextField priceText;
    public ChoiceBox<Algorithm> algChoice;

    private Item selectedItem = null;

    @FXML
    public void initialize() {
        priceInputCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        weightInputCol.setCellValueFactory(new PropertyValueFactory<>("weight"));

        priceOutputCol.setCellValueFactory(new PropertyValueFactory<>("price"));
        outputWeightCol.setCellValueFactory(new PropertyValueFactory<>("weight"));

        priceText.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d")) {
                priceText.setText(newValue.replaceAll("[^\\d]", ""));
                if (selectedItem != null) {
                    selectedItem.setPrice(Integer.parseInt(priceText.getText()));
                    inputTable.refresh();
                }
            }
        });

        weightText.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d")) {
                weightText.setText(newValue.replaceAll("[^\\d]", ""));
                if (selectedItem != null) {
                    selectedItem.setWeight(Integer.parseInt(weightText.getText()));
                    inputTable.refresh();
                }
            }
        });

        capacityText.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d")) {
                capacityText.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        inputTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selectedItem = inputTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                priceText.setText(selectedItem.getPrice().toString());
                weightText.setText(selectedItem.getWeight().toString());
            }
        });

        algChoice.setItems(FXCollections.observableList(Arrays.asList(Algorithm.values())));
    }

    public void addNewItem(ActionEvent actionEvent) {
        Item item = new Item(0, 0);
        inputTable.getItems().add(item);
    }

    public void deleteItem(ActionEvent actionEvent) {
        if (selectedItem != null) {
            inputTable.getItems().remove(selectedItem);
        }
    }

    public void loadFromFile(ActionEvent actionEvent) {
        File file = new File("input.txt");
        Scanner sc;
        List<Item> items = new ArrayList<>();
        try {
            sc = new Scanner(file);
            while (sc.hasNextLine()) {
                String nextLine = sc.nextLine();
                String[] strings = nextLine.split(" ", 2);
                items.add(new Item(Integer.parseInt(strings[0]), Integer.parseInt(strings[1])));
            }
            inputTable.setItems(FXCollections.observableArrayList(items));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setSolvedItems(int[] indexes) {
        List<Item> outputList = new ArrayList<>();
        for (int index : indexes) {
            outputList.add(inputTable.getItems().get(index));
        }
        outputTable.setItems(FXCollections.observableList(outputList));
    }

    public void solve(ActionEvent actionEvent) {
        Algorithm selected = algChoice.getSelectionModel().getSelectedItem();
        try {
            int capacity = Integer.parseInt(capacityText.getText());
            ObservableList<Item> items = inputTable.getItems();
            int[] widths = new int[items.size()];
            int[] prices = new int[items.size()];
            for (int i = 0; i < items.size(); i++) {
                widths[i] = items.get(i).getWeight();
                prices[i] = items.get(i).getPrice();
            }
            if (selected != null) {
                switch (selected) {
                    case FAST:
                        setSolvedItems(BackpackFast.solve(capacity, widths, prices));
                        break;
                    case SLOW:
                        setSolvedItems(BackpackSlow.solve(capacity, widths, prices));
                        break;
                }
            } else {
                new Alert(AlertType.ERROR, "Не выбран алгоритм").show();
            }
        } catch (NumberFormatException ex) {
            new Alert(AlertType.ERROR, "Неверный формат входных данных").show();
        }
    }
}

