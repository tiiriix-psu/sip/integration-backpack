package ru.fnight.integrationbackpack.domains;

public class Item {

    private Integer price;
    private Integer weight;

    public Item(Integer price, Integer weight) {
        this.price = price;
        this.weight = weight;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }
}
